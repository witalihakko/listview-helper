unit ListViewHelper;

interface

uses
  System.SysUtils, System.Classes, Winapi.CommCtrl,
  Vcl.ComCtrls;

type
  TStates = (stInsert,stUpdate,stDelete,stNone);

  TOperator = (opEqual,opGreater,opLower);

  TCriteria = class
      FColumn : string;
      FValue  : string;
      FOperator : TOperator;
    public
      function AddColumn(s:string):TCriteria;
      function AddValue(s:string):TCriteria;
      function SelectOperator(o:TOperator):TCriteria;
      class function CreateCriteria:TCriteria;
  end;

  TStatus = class
    FDeleted : Boolean;
    FInserted : Boolean;
    FUpdated : Boolean;
    FNone : Boolean;
  end;

  //TSearchInfo = (siCaseInSensetive);

  TListViewClassHelper = class helper for TListView
    procedure Loop(const Proc:TProc<TListItem> = nil);
    procedure LoopC(Criteria : TCriteria;const Proc:TProc<TListItem> = nil);
    procedure LoopS(State : TStates;const Proc:TProc<TListItem> = nil);
    procedure FullTextSearch(Tree:TTreeView;SearchText:string);
  end;

const
  IsimArr : array[0..5] of string = ('Hakan','Selim','Merve','Taci','Zeliha','Sakine');
  SoyadArr : array[0..5] of string = ('U�ar','Bilir','Akdeniz','Kalkavan','Durmu�','Dertli');

  OperatorArr : array[0..2] of TOperator = (opEqual,opGreater,opLower);

implementation


{ TCriteria }

function TCriteria.AddColumn(s: string): TCriteria;
begin
  FColumn := s;
  Result := Self;
end;

function TCriteria.AddValue(s: string): TCriteria;
begin
  FValue := s;
  Result := Self;
end;

class function TCriteria.CreateCriteria: TCriteria;
begin
  Result := TCriteria.Create;
end;

function TCriteria.SelectOperator(o: TOperator): TCriteria;
begin
  FOperator := o;
  Result := Self;
end;
{ TCriteria End}

{ TListViewClassHelper }

procedure TListViewClassHelper.FullTextSearch(Tree: TTreeView;
  SearchText: string);
var
  i : Integer;
  Criter : TCriteria;
  SearchCount : Integer;
  SearchParent,SearchNode : TTreeNode;
begin
  for i := 0 to Columns.Count-1 do
    begin
      SearchCount := 0;

      Criter := TCriteria
                  .CreateCriteria
                    .AddColumn(Column[i].Caption)
                    .AddValue(SearchText)
                    .SelectOperator(opEqual);

      if Tree.Items.Count = 0 then
        SearchParent := Tree.Items.AddFirst(nil,'Aranan Kolon: "'+Criter.FColumn+'" - De�er "'+UpperCase(Criter.FValue)+'"')
      else
        SearchParent := Tree.Items.Add(Tree.TopItem,'Aranan Kolon: "'+Criter.FColumn+'" - De�er: "'+UpperCase(Criter.FValue)+'"');

      LoopC(
             Criter,
              procedure (A:TListItem)
              var i : Integer;
               SearchText :string;
              begin
                if SearchCount = 0 then
                  SearchNode := Tree.Items.AddChild(SearchParent,'Bulunanlar');
                Inc(SearchCount);

                for i := 0 to A.SubItems.Count-1 do
                  SearchText := SearchText + ','+A.SubItems[i];

                Tree.Items.AddChild(SearchNode,'Sat�r no :'+IntToStr(A.Index)+SearchText).Data := A;
              end
            );

      if SearchCount = 0 then
        begin
          SearchParent.Free;
        end
      else
        begin
          SearchParent.Text := SearchParent.Text +' ('+ IntToStr(SearchCount)+' tane de�er bulundu)';
          Tree.FullCollapse;
          SearchParent.Expand(True);
        end;
    end;
end;

procedure TListViewClassHelper.Loop(const Proc: TProc<TListItem>);
var i : Integer;
begin
  for i := 0 to Self.Items.Count-1 do
    begin
      Proc(Items[i]);
    end;
end;

procedure TListViewClassHelper.LoopC(Criteria: TCriteria;
  const Proc: TProc<TListItem>);
var
  i,ColIndex : Integer;

  function GetColumnIDByName(ColumnName:string):Integer;
  var ix : Integer;
  begin
    for ix := 0 to Columns.Count-1 do
      if Column[ix].Caption = ColumnName then
        begin
          Result := ix;
          Exit;
        end;
    Result := -1;
  end;
begin
  ColIndex := GetColumnIDByName(Criteria.FColumn);
  if ColIndex < 0 then Exit;

  for i := 0 to Items.Count-1 do
    begin
      if ColIndex = 0 then
        begin
          case Criteria.FOperator of
            opEqual :
              if UpperCase(Items[i].Caption) = UpperCase(Criteria.FValue) then
                Proc(Items[i]);
            opGreater :
              if Items[i].Caption > Criteria.FValue then
                Proc(Items[i]);
            opLower :
              if Items[i].Caption < Criteria.FValue then
                Proc(Items[i]);
          end;
        end
      else
        begin
          case Criteria.FOperator of
            opEqual :
              if UpperCase(Items[i].SubItems[ColIndex-1]) = UpperCase(Criteria.FValue) then
                Proc(Items[i]);
            opGreater :
              if Items[i].SubItems[ColIndex-1] > Criteria.FValue  then
                Proc(Items[i]);
            opLower :
              if Items[i].SubItems[ColIndex-1] < Criteria.FValue then
                Proc(Items[i]);
          end;
        end;
    end;
end;

procedure TListViewClassHelper.LoopS(State: TStates;
  const Proc: TProc<TListItem>);
var
  i : integer;
begin
  for i := Items.Count-1 downto 0 do
    begin
      case State of
        stInsert :
          if TStatus(Items[i].Data).FInserted then
            Proc(Items[i]);
        stUpdate:
          if TStatus(Items[i].Data).FUpdated then
            Proc(Items[i]);
        stDelete:
          if TStatus(Items[i].Data).FDeleted then
            Proc(Items[i]);
        stNone:
          if TStatus(Items[i].Data).FNone then
            Proc(Items[i]);
      end;
    end;
end;
{ TListViewClassHelper End}



end.
