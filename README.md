# Açıklamalar #

Bu döküman listview üzerinden class helper hakkında bilgiler içermektedir.

### İçinde neler var? ###

* Listview üzerinden class helper kullanımı

Class Helper classı:
```
#!pascal

 TListViewClassHelper = class helper for TListView
    procedure Loop(const Proc:TProc<TListItem> = nil);
    procedure LoopC(Criteria : TCriteria;const Proc:TProc<TListItem> = nil);
    procedure LoopS(State : TStates;const Proc:TProc<TListItem> = nil);
    procedure FullTextSearch(Tree:TTreeView;SearchText:string);
  end;
```

### Nasıl kullanılır? ###

* Örnek demoda nasıl kullanılacağı gösterilmiştir.

Örnek kullanım kodu:


```
#!pascal

  Listview
            .Loops(
                   stDelete,
                    procedure (A:TListItem)
                    var qry : TADOQuery;
                    begin
//                      try
//                        try
//                          qry := TADOQuery.Create(nil);
//                          qry.Connection := Conn; //adoconnection bağlantısı olan component
//
//                          qry.SQL.Text := 'Delete Contacts where ID=:ID';
//                          qry.Parameters.ParamByName('ID').Value := A.Caption;
//                          qry.ExecSQL;
//                        except
//                          raise Exception.Create('Silme işleminde hata oluştu.');
//                        end;
//                      finally
//                        qry.Free;
//                      end;

                      A.Free;
                    end
                  );
```

Demo önizleme:

![ListviewClassHelper2.gif](https://bitbucket.org/repo/ekrgdjB/images/3085903929-ListviewClassHelper2.gif)