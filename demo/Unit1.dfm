object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'ListView Loop Helper'
  ClientHeight = 412
  ClientWidth = 605
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 281
    Width = 605
    Height = 3
    Cursor = crVSplit
    Align = alBottom
    Visible = False
    ExplicitTop = 35
    ExplicitWidth = 223
  end
  object lvContacts: TListView
    Left = 0
    Top = 65
    Width = 605
    Height = 216
    Align = alClient
    Columns = <
      item
        Caption = 'S'#305'ra No'
      end
      item
        Caption = 'Ad'#305
        Width = 100
      end
      item
        Caption = 'Soyad'#305
        Width = 150
      end
      item
        Caption = 'Ya'#351#305
      end>
    RowSelect = True
    TabOrder = 0
    ViewStyle = vsReport
    OnAdvancedCustomDrawItem = lvContactsAdvancedCustomDrawItem
    OnKeyDown = lvContactsKeyDown
    ExplicitWidth = 604
    ExplicitHeight = 190
  end
  object Panel1: TPanel
    Left = 0
    Top = 375
    Width = 605
    Height = 37
    Align = alBottom
    BevelKind = bkTile
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 349
    ExplicitWidth = 604
    DesignSize = (
      601
      33)
    object btnLoad: TButton
      Left = 64
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Y'#252'kle'
      TabOrder = 0
      OnClick = btnLoadClick
    end
    object seCount: TSpinEdit
      Left = 8
      Top = 4
      Width = 57
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 1
      Value = 15
    end
    object pbLoading: TProgressBar
      Left = 226
      Top = 8
      Width = 358
      Height = 17
      Anchors = [akLeft, akTop, akRight]
      Step = 1
      TabOrder = 2
      ExplicitWidth = 357
    end
    object btnDelete: TButton
      Left = 145
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Sil'
      TabOrder = 3
      OnClick = btnDeleteClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 605
    Height = 35
    Align = alTop
    TabOrder = 2
    Visible = False
    ExplicitWidth = 604
    object lblColumn: TLabel
      Left = 10
      Top = 11
      Width = 26
      Height = 13
      Caption = 'Kolon'
    end
    object lblValue: TLabel
      Left = 169
      Top = 11
      Width = 29
      Height = 13
      Caption = 'De'#287'er'
    end
    object lblOperator: TLabel
      Left = 352
      Top = 11
      Width = 44
      Height = 13
      Caption = 'Operat'#246'r'
    end
    object cbbOperator: TComboBox
      Left = 402
      Top = 8
      Width = 111
      Height = 21
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 0
      Text = 'E'#351'it'
      Items.Strings = (
        'E'#351'it'
        'B'#252'y'#252'k'
        'K'#252#231#252'k')
    end
    object edtValue: TEdit
      Left = 204
      Top = 8
      Width = 121
      Height = 21
      TabOrder = 1
      Text = 'U'
    end
    object cbbColumn: TComboBox
      Left = 48
      Top = 8
      Width = 115
      Height = 21
      Style = csDropDownList
      TabOrder = 2
    end
    object btnSearch: TButton
      Left = 519
      Top = 6
      Width = 75
      Height = 24
      Caption = 'Search'
      TabOrder = 3
      OnClick = btnSearchClick
    end
  end
  object trvSearch: TTreeView
    Left = 0
    Top = 284
    Width = 605
    Height = 91
    Align = alBottom
    Indent = 19
    RowSelect = True
    TabOrder = 3
    Visible = False
    OnDblClick = trvSearchDblClick
    ExplicitTop = 258
    ExplicitWidth = 604
  end
  object pnlSearchPanel: TPanel
    Left = 0
    Top = 35
    Width = 605
    Height = 30
    Align = alTop
    BevelKind = bkTile
    BevelOuter = bvNone
    TabOrder = 4
    Visible = False
    ExplicitWidth = 604
    DesignSize = (
      601
      26)
    object edtSearchText: TEdit
      Left = 34
      Top = 3
      Width = 555
      Height = 21
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
      OnKeyPress = edtSearchTextKeyPress
      ExplicitWidth = 554
    end
    object btnCloseSearch: TButton
      Left = 0
      Top = 1
      Width = 31
      Height = 25
      Caption = 'X'
      TabOrder = 1
      OnClick = btnCloseSearchClick
    end
  end
end
