unit Unit1;

interface

uses
  Winapi.Windows, System.SysUtils, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.ComCtrls, Vcl.Samples.Spin, Data.Win.ADODB;

type
  TForm1 = class(TForm)
    lvContacts: TListView;
    Panel1: TPanel;
    btnLoad: TButton;
    seCount: TSpinEdit;
    pbLoading: TProgressBar;
    Panel2: TPanel;
    cbbOperator: TComboBox;
    lblColumn: TLabel;
    lblValue: TLabel;
    edtValue: TEdit;
    lblOperator: TLabel;
    cbbColumn: TComboBox;
    trvSearch: TTreeView;
    Splitter1: TSplitter;
    btnSearch: TButton;
    btnDelete: TButton;
    pnlSearchPanel: TPanel;
    edtSearchText: TEdit;
    btnCloseSearch: TButton;
    procedure btnLoadClick(Sender: TObject);
    procedure btnSearchClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lvContactsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure trvSearchDblClick(Sender: TObject);
    procedure lvContactsAdvancedCustomDrawItem(Sender: TCustomListView;
      Item: TListItem; State: TCustomDrawState; Stage: TCustomDrawStage;
      var DefaultDraw: Boolean);
    procedure btnDeleteClick(Sender: TObject);
    procedure edtSearchTextKeyPress(Sender: TObject; var Key: Char);
    procedure btnCloseSearchClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  ListViewHelper;

{$R *.dfm}

procedure TForm1.btnLoadClick(Sender: TObject);
var
 AlvItem : TListItem;
 Ix : Integer;
 st : TStatus;
begin
  lvContacts.Clear;
  trvSearch.Items.Clear;
  pbLoading.Max := seCount.Value;
  pbLoading.Position := 0;

  for Ix := 0 to seCount.Value do
    begin
      st := TStatus.Create;
      AlvItem := lvContacts.Items.Add;
      with AlvItem do
        begin
          Caption := IntToStr(Ix);
          SubItems.Add(IsimArr[Random(6)]);
          SubItems.Add(SoyadArr[Random(6)]);
          SubItems.Add(IntToStr(Random(50)));

          Data := st;
        end;
      pbLoading.StepBy(1);
    end;
end;

procedure TForm1.btnSearchClick(Sender: TObject);
var
  Criter : TCriteria;
  SearchCount : Integer;
  SearchParent,SearchNode : TTreeNode;
begin
  SearchCount := 0;

  Criter := TCriteria
              .CreateCriteria
                .AddColumn(cbbColumn.Text)
                .AddValue(edtValue.Text)
                .SelectOperator(OperatorArr[cbbOperator.ItemIndex]);

  trvSearch.Visible := True;
  Splitter1.Visible := True;

  if trvSearch.Items.Count = 0 then
    SearchParent := trvSearch.Items.AddFirst(nil,'Aranan "'+UpperCase(Criter.FValue)+'"')
  else
    SearchParent := trvSearch.Items.Add(trvSearch.TopItem,'Aranan "'+UpperCase(Criter.FValue)+'"');

  lvContacts
            .LoopC(
                   Criter,
                    procedure (A:TListItem)
                    var i : Integer;
                     SearchText :string;
                    begin
                      if SearchCount = 0 then
                        SearchNode := trvSearch.Items.AddChild(SearchParent,'Bulunanlar');
                      Inc(SearchCount);

                      for i := 0 to A.SubItems.Count-1 do
                        SearchText := SearchText + ','+A.SubItems[i];

                      trvSearch.Items.AddChild(SearchNode,'Sat�r no :'+IntToStr(A.Index)+SearchText).Data := A;
                    end
                  );
  SearchParent.Text := SearchParent.Text +'('+ IntToStr(SearchCount)+' tane de�er bulundu)';
  trvSearch.FullCollapse;
  SearchParent.Expand(True);

  Criter.Free; 
end;

procedure TForm1.edtSearchTextKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    begin
      lvContacts.FullTextSearch(trvSearch,edtSearchText.Text);
      trvSearch.Visible := True;
      Splitter1.Visible := True;
    end;
end;

procedure TForm1.FormCreate(Sender: TObject);
var i : integer;
begin
  for i := 0 to lvContacts.Columns.Count-1 do
    cbbColumn.Items.Add(lvContacts.Column[i].Caption);

  cbbColumn.ItemIndex := 0;
end;

procedure TForm1.lvContactsAdvancedCustomDrawItem(Sender: TCustomListView;
  Item: TListItem; State: TCustomDrawState; Stage: TCustomDrawStage;
  var DefaultDraw: Boolean);
begin
  if Assigned(Item.Data) then
    if TStatus(Item.Data).FDeleted then
      Sender.Canvas.Brush.Color := clGray
    else
      Sender.Canvas.Brush.Color := clWhite;
end;

procedure TForm1.lvContactsKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (ssCtrl in Shift) and (Key = ord('F')) then
    begin
      pnlSearchPanel.Visible := True;    
      edtSearchText.SetFocus;
    end;                                 
    
  if Assigned((Sender as TListView).Selected) then
    if Assigned((Sender as TListView).Selected.Data) then
      if (key = VK_DELETE)then
        if (TStatus((Sender as TListView).Selected.Data).FDeleted) then
          TStatus((Sender as TListView).Selected.Data).FDeleted := False
        else
          TStatus((Sender as TListView).Selected.Data).FDeleted := True;
end;

procedure TForm1.trvSearchDblClick(Sender: TObject);
begin
  if Assigned(trvSearch.Selected.Data) then
    begin
      lvContacts.ItemIndex := TListItem(trvSearch.Selected.Data).Index;
      lvContacts.Selected.MakeVisible(true);
    end;
end;

procedure TForm1.btnCloseSearchClick(Sender: TObject);
begin
  pnlSearchPanel.Visible := False;
  trvSearch.Visible := False;
  Splitter1.Visible := False;
end;

procedure TForm1.btnDeleteClick(Sender: TObject);
begin
  lvContacts
            .Loops(
                   stDelete,
                    procedure (A:TListItem)
                    var qry : TADOQuery;
                    begin
//                      try
//                        try
//                          qry := TADOQuery.Create(nil);
//                          qry.Connection := Conn; //adoconnection ba�lant�s� olan component
//
//                          qry.SQL.Text := 'Delete Contacts where ID=:ID';
//                          qry.Parameters.ParamByName('ID').Value := A.Caption;
//                          qry.ExecSQL;
//                        except
//                          raise Exception.Create('Silme i�leminde hata olu�tu.');
//                        end;
//                      finally
//                        qry.Free;
//                      end;

                      A.Free;
                    end
                  );
end;


end.
